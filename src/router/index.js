import Vue from 'vue'
import Router from 'vue-router'
import Checkin from '@/components/Checkin.vue'
import Login from '@/components/Login.vue'
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Checkin',
      component: Checkin
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    }
  ]
})
