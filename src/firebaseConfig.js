import firebase from 'firebase'
import 'firebase/firestore'

let config = {
  apiKey: "AIzaSyC7lfKToqlc65h88fDfRveitBUC8AFoeFM",
  authDomain: "creative-tomorrow-backend.firebaseapp.com",
  databaseURL: "https://creative-tomorrow-backend.firebaseio.com",
  projectId: "creative-tomorrow-backend",
  storageBucket: "creative-tomorrow-backend.appspot.com",
  messagingSenderId: "366898760403"
};

firebase.initializeApp(config);

// firebase utils
const db = firebase.firestore();
const auth = firebase.auth();
const currentUser = auth.currentUser;

// date issue fix according to firebase
const settings = {
    timestampsInSnapshots: true
}
db.settings(settings);

// firebase collections
const thoughtsCollection = db.collection('thoughts');
const eventsCollection = db.collection('events');

export {
    db,
    auth,
    currentUser,
    thoughtsCollection,
    eventsCollection
}